package cr.ac.ucr.ecci.eseg.androidstorage;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class UtilFiles {

    public static void guardarArchivoInterno(Context contexto, String nombre, String contenido) {
        try {
            FileOutputStream fos = contexto.openFileOutput(nombre, Context.MODE_PRIVATE);
            fos.write(contenido.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String leerArchivoInterno(Context contexto, String nombre) {
        StringBuilder readString = new StringBuilder();
        try {
            FileInputStream fis = contexto.openFileInput(nombre);
            int size;
            while ((size = fis.read()) != -1) {
                readString.append((char) size);
            }
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return readString.toString();
    }
}