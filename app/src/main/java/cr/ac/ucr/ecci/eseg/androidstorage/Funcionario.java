package cr.ac.ucr.ecci.eseg.androidstorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.Date;

public class Funcionario extends Persona {
    private int unidadBase;
    private int puestoBase;
    private double salarioBase;

    public Funcionario(){
        super();
    }

    public Funcionario(String identificacion, String correo, String nombre, String primerApellido, String segundoApellido, String telefono, String celular, Date fechaNacimiento, String tipo, String genero, int unidadBase, int puestoBase, double salarioBase) {
        super(identificacion, correo, nombre, primerApellido, segundoApellido, telefono, celular, fechaNacimiento, tipo, genero);
        this.setUnidadBase(unidadBase);
        this.setPuestoBase(puestoBase);
        this.setSalarioBase(salarioBase);
    }

    public Funcionario(String identificacion, String correo, String nombre, String primerApellido, String segundoApellido, String telefono, String celular, Date fechaNacimiento, String tipo, String genero) {
        super(identificacion, correo, nombre, primerApellido, segundoApellido, telefono, celular, fechaNacimiento, tipo, genero);
    }

    protected Funcionario(Parcel in){
        super(in);
    }

    public Funcionario(Parcel in, int unidadBase, int puestoBase, double salarioBase) {
        super(in);
        this.unidadBase = unidadBase;
        this.puestoBase = puestoBase;
        this.salarioBase = salarioBase;
    }

    public int getUnidadBase() {
        return unidadBase;
    }

    public void setUnidadBase(int unidadBase) {
        this.unidadBase = unidadBase;
    }

    public int getPuestoBase() {
        return puestoBase;
    }

    public void setPuestoBase(int puestoBase) {
        this.puestoBase = puestoBase;
    }

    public double getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(double salarioBase) {
        this.salarioBase = salarioBase;
    }

    public long insertar(Context context) {
        long newRowId = super.insertar(context);
        if (newRowId > 0) {
            DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
            SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DataBaseContract.DataBaseEntry._ID, getIdentificacion());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_BASE, getUnidadBase());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUESTO_BASE, getPuestoBase());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_SALARIO_BASE, getSalarioBase());
            newRowId = db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_FUNCIONARIO, null, values);
        }
        return newRowId;
    }

    public void leer (Context context, String identificacion){
        super.leer(context, identificacion);
        if (getTipo().equals(Persona.TIPO_ADMINISTRATIVO) || getTipo().equals(Persona.TIPO_PROFESOR) || getTipo().equals(Persona.TIPO_EXTERNO)) {
            DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String[] projection = {
                    DataBaseContract.DataBaseEntry._ID,
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_BASE,
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_PUESTO_BASE,
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_SALARIO_BASE
            };
            String selection = DataBaseContract.DataBaseEntry._ID + " = ?";
            String[] selectionArgs = {identificacion};
            Cursor cursor = db.query(
                    DataBaseContract.DataBaseEntry.TABLE_NAME_FUNCIONARIO, // tabla
                    projection, // columnas
                    selection, // where
                    selectionArgs, // valores del where
                    null, // agrupamiento
                    null, // filtros por grupo
                    null // orden
            );
            if (cursor.moveToFirst() && cursor.getCount() > 0) {
                setUnidadBase(cursor.getInt(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_BASE
                )));
                setPuestoBase(cursor.getInt(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_NAME_PUESTO_BASE
                )));
                setSalarioBase(cursor.getDouble(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_NAME_SALARIO_BASE
                )));
            }
            cursor.close();
        }
    }

    public void eliminar (Context context, String identificacion) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        String selection = DataBaseContract.DataBaseEntry._ID + " LIKE ?";
        String[] selectionArgs = {identificacion};
        db.delete(DataBaseContract.DataBaseEntry.TABLE_NAME_FUNCIONARIO, selection, selectionArgs);
        super.eliminar(context, identificacion);
    }

    public int actualizar(Context context) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_BASE, getUnidadBase());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUESTO_BASE, getPuestoBase());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_SALARIO_BASE, getSalarioBase());
        String selection = DataBaseContract.DataBaseEntry._ID + " LIKE ?";
        String[] selectionArgs = {getIdentificacion()};
        int contador = db.update(DataBaseContract.DataBaseEntry.TABLE_NAME_FUNCIONARIO, values, selection, selectionArgs);
        contador += super.actualizar(context);
        return contador;
    }
}
