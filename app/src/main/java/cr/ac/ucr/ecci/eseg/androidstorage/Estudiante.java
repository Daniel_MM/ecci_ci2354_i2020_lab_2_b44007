package cr.ac.ucr.ecci.eseg.androidstorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.Date;

public class Estudiante extends Persona {
    private String carnet;
    private int carreraBase;
    private double promedioPonderado;

    public Estudiante() {
        super();
    }

    public Estudiante(String identificacion, String correo, String nombre, String primerApellido, String segundoApellido, String telefono, String celular, Date fechaNacimiento, String tipo, String genero, String carnet, int carreraBase, double promedioPonderado) {
        super(identificacion, correo, nombre, primerApellido, segundoApellido, telefono, celular, fechaNacimiento, tipo, genero);
        this.carnet = carnet;
        this.carreraBase = carreraBase;
        this.promedioPonderado = promedioPonderado;
    }

    public Estudiante(String identificacion, String correo, String nombre, String primerApellido, String segundoApellido, String telefono, String celular, Date fechaNacimiento, String tipo, String genero) {
        super(identificacion, correo, nombre, primerApellido, segundoApellido, telefono, celular, fechaNacimiento, tipo, genero);
    }

    public Estudiante(Parcel in, String carnet, int carreraBase, double promedioPonderado) {
        super(in);
        this.carnet = carnet;
        this.carreraBase = carreraBase;
        this.promedioPonderado = promedioPonderado;
    }

    public Estudiante(Parcel in) {
        super(in);
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public int getCarreraBase() {
        return carreraBase;
    }

    public void setCarreraBase(int carreraBase) {
        this.carreraBase = carreraBase;
    }

    public double getPromedioPonderado() {
        return promedioPonderado;
    }

    public void setPromedioPonderado(double promedioPonderado) {
        this.promedioPonderado = promedioPonderado;
    }

    public long insertar(Context context) {
        long newRowId = super.insertar(context);
        if (newRowId > 0) {
            DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
            SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DataBaseContract.DataBaseEntry._ID, getIdentificacion());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_CARNET, getCarnet());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_CARRERA_BASE, getCarreraBase());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PROMEDIO_PONDERADO, getPromedioPonderado());
            newRowId = db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_ESTUDIANTE, null, values);
        }
        return newRowId;
    }

    public void leer (Context context, String identificacion){
        super.leer(context, identificacion);
        if (getTipo().equals(Persona.TIPO_ESTUDIANTE)) {
            DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            String[] projection = {
                    DataBaseContract.DataBaseEntry._ID,
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_CARNET,
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_CARRERA_BASE,
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_PROMEDIO_PONDERADO
            };
            String selection = DataBaseContract.DataBaseEntry._ID + " = ?";
            String[] selectionArgs = {identificacion};
            Cursor cursor = db.query(
                    DataBaseContract.DataBaseEntry.TABLE_NAME_ESTUDIANTE, // tabla
                    projection, // columnas
                    selection, // where
                    selectionArgs, // valores del where
                    null, // agrupamiento
                    null, // filtros por grupo
                    null // orden
            );
            if (cursor.moveToFirst() && cursor.getCount() > 0) {
                setCarnet(cursor.getString(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_NAME_CARNET
                )));
                setCarreraBase(cursor.getInt(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_NAME_CARRERA_BASE
                )));
                setPromedioPonderado(cursor.getDouble(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_NAME_PROMEDIO_PONDERADO
                )));
            }
            cursor.close();
        }
    }

    public void eliminar (Context context, String identificacion) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        String selection = DataBaseContract.DataBaseEntry._ID + " LIKE ?";
        String[] selectionArgs = {identificacion};
        db.delete(DataBaseContract.DataBaseEntry.TABLE_NAME_ESTUDIANTE, selection, selectionArgs);
        super.eliminar(context, identificacion);
    }

    public int actualizar(Context context) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_CARNET, getCarnet());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_CARRERA_BASE,getCarreraBase());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PROMEDIO_PONDERADO,getPromedioPonderado());
        String selection = DataBaseContract.DataBaseEntry._ID + " LIKE ?";
        String[] selectionArgs = {getIdentificacion()};
        int contador = db.update(DataBaseContract.DataBaseEntry.TABLE_NAME_ESTUDIANTE, values, selection, selectionArgs);
        contador += super.actualizar(context);
        return contador;
    }
}
