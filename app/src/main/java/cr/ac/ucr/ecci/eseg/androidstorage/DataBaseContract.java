package cr.ac.ucr.ecci.eseg.androidstorage;

import android.provider.BaseColumns;
//TODO: comparar este lab con room
public final class DataBaseContract {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    private DataBaseContract() {}

    public static class DataBaseEntry implements BaseColumns {
        public static final String TABLE_NAME_PERSONA           = "Persona";
        public static final String COLUMN_NAME_CORREO           = "correo";
        public static final String COLUMN_NAME_NOMBRE           = "nombre";
        public static final String COLUMN_NAME_PRIMER_APELLIDO  = "primerApellido";
        public static final String COLUMN_NAME_SEGUNDO_APELLIDO = "segundoApellido";
        public static final String COLUMN_NAME_TELEFONO         = "telefono";
        public static final String COLUMN_NAME_CELULAR          = "celular";
        public static final String COLUMN_NAME_FECHA_NACIMIENTO = "fechaNacimiento";
        public static final String COLUMN_NAME_TIPO             = "tipo";
        public static final String COLUMN_NAME_GENERO           = "genero";

        public static final String TABLE_NAME_ESTUDIANTE = "Estudiante";
        public static final String COLUMN_NAME_CARNET = "carnet";
        public static final String COLUMN_NAME_CARRERA_BASE = "carreraBase";
        public static final String COLUMN_NAME_PROMEDIO_PONDERADO = "promedioPonderado";

        public static final String TABLE_NAME_FUNCIONARIO = "Funcionario";
        public static final String COLUMN_NAME_UNIDAD_BASE = "unidadBase";
        public static final String COLUMN_NAME_PUESTO_BASE = "puestoBase";
        public static final String COLUMN_NAME_SALARIO_BASE = "salarioBase";
    }

    public static final String SQL_CREATE_PERSONA =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_PERSONA + " (" +
                    DataBaseEntry._ID                           + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_CORREO            + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_NOMBRE            + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PRIMER_APELLIDO   + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_SEGUNDO_APELLIDO  + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_TELEFONO          + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_CELULAR           + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_FECHA_NACIMIENTO  + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_TIPO              + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_GENERO            + TEXT_TYPE + " )";
    public static final String SQL_DELETE_PERSONA =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_PERSONA;

    public static final String SQL_CREATE_ESTUDIANTE =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_ESTUDIANTE + " (" +
                    DataBaseEntry._ID                               + TEXT_TYPE     + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_CARNET                + TEXT_TYPE     + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_CARRERA_BASE          + INTEGER_TYPE  + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PROMEDIO_PONDERADO    + REAL_TYPE     + COMMA_SEP +
                    "FOREIGN KEY(" + DataBaseEntry._ID + ") REFERENCES " + DataBaseEntry.TABLE_NAME_PERSONA + "(" + DataBaseEntry._ID + "))";
    public static final String SQL_DELETE_ESTUDIANTE =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_ESTUDIANTE;

    public static final String SQL_CREATE_FUNCIONARIO =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_FUNCIONARIO + " (" +
                    DataBaseEntry._ID                       + TEXT_TYPE     + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_UNIDAD_BASE   + INTEGER_TYPE  + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PUESTO_BASE   + INTEGER_TYPE  + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_SALARIO_BASE  + REAL_TYPE     + COMMA_SEP +
                    "FOREIGN KEY(" + DataBaseEntry._ID + ") REFERENCES " + DataBaseEntry.TABLE_NAME_PERSONA + "(" + DataBaseEntry._ID + "))";
    public static final String SQL_DELETE_FUNCIONARIO =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_FUNCIONARIO;
}