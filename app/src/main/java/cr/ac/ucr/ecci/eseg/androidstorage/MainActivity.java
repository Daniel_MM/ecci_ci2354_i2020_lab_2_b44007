package cr.ac.ucr.ecci.eseg.androidstorage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Button buttonDataBaseInsert = findViewById(R.id.buttonDataBaseInsert);
        Button buttonDataBaseUpdate = findViewById(R.id.buttonDataBaseUpdate);
        Button buttonDataBaseDelete = findViewById(R.id.buttonDataBaseDelete);
        Button buttonDataBaseSelect = findViewById(R.id.buttonDataBaseSelect);
        Button buttonGrabarArchivo = findViewById(R.id.buttonGrabarArchivo);
        Button buttonLeerArchivo = findViewById(R.id.buttonLeerArchivo);

        buttonDataBaseInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertarEstudiante();
            }
        });

        buttonDataBaseUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizarEstudiante();
            }
        });

        buttonDataBaseDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarEstudiante();
            }
        });

        buttonDataBaseSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leerEstudiante();
            }
        });

        buttonGrabarArchivo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                grabarArchivo();
            }
        });
        buttonLeerArchivo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                leerArchivo();
            }
        });
    }

    private void insertarEstudiante() {
        // Instancia la clase Estudiante y realiza la inserción de datos
        Estudiante estudiante = new Estudiante(
                "1-1000-1000",
                "estudiante01@ucr.ac.cr",
                "Juan",
                "Perez",
                "Soto",
                "2511-0000",
                "8890-0000",
                UtilDates.StringToDate("01 01 1995"),
                Persona.TIPO_ESTUDIANTE,
                Persona.GENERO_MASCULINO,
                "A99148",
                1,
                8.0
        );
        long newRowId = estudiante.insertar(getApplicationContext());

        Toast.makeText(
                getApplicationContext(),
                "Insertar Estudiante: " + newRowId +
                " Id: "         + estudiante.getIdentificacion() +
                " Carnet: "     + estudiante.getCarnet() +
                " Nombre: "     + estudiante.getNombre() + " " + estudiante.getPrimerApellido() + " " + estudiante.getSegundoApellido() +
                " Correo: "     + estudiante.getCorreo() +
                " Tipo: "       + estudiante.getTipo() +
                " Promedio: "   + estudiante.getPromedioPonderado(),
                Toast.LENGTH_LONG
        ).show();
    }

    private void actualizarEstudiante() {
        Estudiante estudiante = new Estudiante(
                "1-1000-1000",
                "estudiante01@ucr.ac.cr*",
                "Juan*",
                "Perez*",
                "Soto*",
                "2511-0000*",
                "8890-0000*",
                UtilDates.StringToDate("01 01 1995"),
                Persona.TIPO_ESTUDIANTE,
                Persona.GENERO_MASCULINO,
                "A99148*",
                1,
                8.0
        );
        int contador = estudiante.actualizar(getApplicationContext());
        if (contador >0) {
            Toast.makeText(
                    getApplicationContext(),
                    "Actualizar Estudiante: " + contador +
                    " Id: "         + estudiante.getIdentificacion() +
                    " Carnet: "     + estudiante.getCarnet() +
                    " Nombre: "     + estudiante.getNombre() + " " + estudiante.getPrimerApellido() + " " + estudiante.getSegundoApellido() +
                    " Correo: "     + estudiante.getCorreo() +
                    " Tipo: "       + estudiante.getTipo() +
                    " Promedio: "   + estudiante.getPromedioPonderado(),
                    Toast.LENGTH_LONG
            ).show();
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    "No hay datos para: " + estudiante.getIdentificacion(),
                    Toast.LENGTH_LONG
            ).show();
        }
    }

    private void eliminarEstudiante() {
        String id = "1-1000-1000";
        Estudiante estudiante = new Estudiante();
        estudiante.eliminar(getApplicationContext(), id);
        Toast.makeText(
                getApplicationContext(),
                "Estudiante eliminado.",
                Toast.LENGTH_LONG
        ).show();
    }

    private void leerEstudiante() {
        String id = "1-1000-1000"; // param
        Estudiante estudiante = new Estudiante();
        estudiante.leer(getApplicationContext(), id);
        if (estudiante.getTipo().equals(Persona.TIPO_ESTUDIANTE)) {
            Toast.makeText(
                    getApplicationContext(),
                    "Leer Estudiante: " + estudiante.getIdentificacion() +
                            " Carnet: "     + estudiante.getCarnet() +
                            " Nombre: "     + estudiante.getNombre() + " " + estudiante.getPrimerApellido() + " " + estudiante.getSegundoApellido() +
                            " Correo: "     + estudiante.getCorreo() +
                            " Tipo: "       + estudiante.getTipo() +
                            " Promedio: "   + estudiante.getPromedioPonderado(),
                    Toast.LENGTH_LONG
            ).show();
        }
        else {
            Toast.makeText(
                    getApplicationContext(),
                    "No hay datos para: " + id,
                    Toast.LENGTH_LONG
            ).show();
        }
    }

    public void grabarArchivo() {
        Persona persona = new Persona(
                "1-1000-1000",
                "estudiante01@ucr.ac.cr",
                "Juan",
                "Perez",
                "Soto*",
                "2511-0000",
                "8890-0000",
                UtilDates.StringToDate("01 01 1995"),
                Persona.TIPO_ESTUDIANTE,
                Persona.GENERO_MASCULINO
        );
        UtilFiles.guardarArchivoInterno(getApplicationContext(),"PersonaAndroidStorage.json", persona.toJson());

        Toast.makeText(getApplicationContext(),"Archivo creado: " + "PersonaAndroidStorage.json", Toast.LENGTH_LONG).show();
    }

    public void leerArchivo() {
        String datosArchivo = UtilFiles.leerArchivoInterno(getApplicationContext(), "PersonaAndroidStorage.json");
        if (datosArchivo.isEmpty()) {
            Toast.makeText(getApplicationContext(), "No hay datos para: " + "PersonaAndroidStorage.json", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Archivo: " + "PersonaAndroidStorage.json" +
                    "Contenido: " + datosArchivo, Toast.LENGTH_LONG
            ).show();
        }
    }
}